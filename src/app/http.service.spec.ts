/* tslint:disable:no-unused-variable */

import { addProviders, async, inject } from '@angular/core/testing';
import { HttpService } from './http.service';
import { Headers, Http, Response, Request, RequestMethod} from '@angular/http';

class MockHttp {};

describe('Service: Http', () => {

	beforeEach(() => {
	    addProviders([
	    	
	        {  provide: Http, useClass: MockHttp}

	    	]);
	    
	 });
 

    it('should ...',
    inject([Http],
      (httpService: HttpService) => {
        expect(httpService).toBeTruthy();
      }));
});
