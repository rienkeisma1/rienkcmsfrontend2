import { Injectable } from '@angular/core';
import { Headers, Http, Response, Request, RequestMethod} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class HttpService {

  	constructor(

  		private http: Http

  	) { }

  	call(url, type, config) {

         // type to lowercase with first as capital
        type = type.trim();
        type = type.charAt(0).toUpperCase() + type.slice(1).toLowerCase();

        if(window['config']['dummmy'] === true || url === null /*config['loadconfigjs'] === true*/){

            return Observable.create(observer => {
                observer.next(this.getMockData(url, type));
                observer.complete();
            });

            // return this.getMockData(url, type);

        }else{

            return this.http.request(new Request({
                method: RequestMethod[type],
                url: url
            })).map((r: Response) => r.json());

        }

    }

    // config can be loaded from the server but the config object is not yet available so config can't be passed in
    getConfig(){

        // window['configsettings']
        if(window['configsettings']['configfromserver'] === true){

            let type = window['configsettings']['type'].trim();
            type = type.charAt(0).toUpperCase() + type.slice(1).toLowerCase();

            return this.http.request(new Request({
                method: RequestMethod[type],
                url: window['configsettings']['url']
            })).map((r: Response) => r.json());


        }else{

            return Observable.create(observer => {
                observer.next(window['config']);
                observer.complete();
            });

        }

    }


    getMockData(url, type) {

        if(url.indexOf('http://local.rienkcmswp.com/') !== -1 && url.indexOf('wp-json/wp/v2/posts/') === -1 && type === 'Get'){

            return  this.getMockList(url);

            // is a post
        }else{ // if('http://local.rienkcmswp.com/wp-json/wp/v2/posts'.indexOf(url)){

            return this.getMockPost(url);
        }

    }

    getMockPost(url): any{

        return {
            "id": 14,
            "date": "2016-08-10T04:05:26",
            "date_gmt": "2016-08-10T02:05:26",
            "guid": {
                "rendered": "http:\/\/local.rienkcmswp.com\/headers-post\/"
            },
            "modified": "2016-08-10T04:05:26",
            "modified_gmt": "2016-08-10T02:05:26",
            "slug": "headers-post",
            "type": "post",
            "link": "http:\/\/local.rienkcmswp.com\/headers-post\/",
            "title": {
                "rendered": "Headers Post"
            },
            "content": {
                "rendered": "<h1>"+url+"This Is An H1 Tag<\/h1>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h2>This Is An H2 Tag<\/h2>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h3>This Is An H3 Tag<\/h3>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h4>This Is An H4 Tag<\/h4>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h5>This Is An H5 Tag<\/h5>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n"
            },
            "excerpt": {
                "rendered": "<p>This Is An H1 Tag Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. This Is An H2 Tag Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed &hellip; <a href=\"http:\/\/local.rienkcmswp.com\/headers-post\/\" class=\"more-link\"><span class=\"screen-reader-text\">&#8220;Headers Post&#8221;<\/span> verder lezen<\/a><\/p>\n"
            },
            "author": 1,
            "featured_media": 0,
            "comment_status": "open",
            "ping_status": "open",
            "sticky": false,
            "format": "standard",
            "categories": [1],
            "tags": [],
            "_links": {
                "self": [{
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/14"
                }],
                "collection": [{
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
                }],
                "about": [{
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
                }],
                "author": [{
                    "embeddable": true,
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
                }],
                "replies": [{
                    "embeddable": true,
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=14"
                }],
                "version-history": [{
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/14\/revisions"
                }],
                "wp:attachment": [{
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=14"
                }],
                "wp:term": [{
                    "taxonomy": "category",
                    "embeddable": true,
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=14"
                }, {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=14"
                }],
                "curies": [{
                    "name": "wp",
                    "href": "https:\/\/api.w.org\/{rel}",
                    "templated": true
                }]
            }
        };

    }

    getMockList(url): any{

    return [{
    "id": 14,
    "date": "2016-08-10T04:05:26",
    "date_gmt": "2016-08-10T02:05:26",
    "guid": {
        "rendered": "http:\/\/local.rienkcmswp.com\/headers-post\/"
    },
    "modified": "2016-08-10T04:05:26",
    "modified_gmt": "2016-08-10T02:05:26",
    "slug": "headers-post",
    "type": "post",
    "link": "http:\/\/local.rienkcmswp.com\/headers-post\/",
    "title": {
        "rendered": "Headers Post"
    },
    "content": {
        "rendered": "<h1>This Is An H1 Tag<\/h1>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h2>This Is An H2 Tag<\/h2>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h3>This Is An H3 Tag<\/h3>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h4>This Is An H4 Tag<\/h4>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<h5>This Is An H5 Tag<\/h5>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n"
    },
    "excerpt": {
        "rendered": "<p>"+url+"This Is An H1 Tag Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. This Is An H2 Tag Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed &hellip; <a href=\"http:\/\/local.rienkcmswp.com\/headers-post\/\" class=\"more-link\"><span class=\"screen-reader-text\">&#8220;Headers Post&#8221;<\/span> verder lezen<\/a><\/p>\n"
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "format": "standard",
    "categories": [1],
    "tags": [],
    "_links": {
        "self": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/14"
        }],
        "collection": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
        }],
        "about": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
        }],
        "author": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
        }],
        "replies": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=14"
        }],
        "version-history": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/14\/revisions"
        }],
        "wp:attachment": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=14"
        }],
        "wp:term": [{
            "taxonomy": "category",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=14"
        }, {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=14"
        }],
        "curies": [{
            "name": "wp",
            "href": "https:\/\/api.w.org\/{rel}",
            "templated": true
        }]
    }
}, {
    "id": 13,
    "date": "2016-08-10T04:05:26",
    "date_gmt": "2016-08-10T02:05:26",
    "guid": {
        "rendered": "http:\/\/local.rienkcmswp.com\/links-post\/"
    },
    "modified": "2016-08-10T04:05:26",
    "modified_gmt": "2016-08-10T02:05:26",
    "slug": "links-post",
    "type": "post",
    "link": "http:\/\/local.rienkcmswp.com\/links-post\/",
    "title": {
        "rendered": "Links Post"
    },
    "content": {
        "rendered": "<p>Lorem ipsum dolor sit amet, <a href=\"#\">consectetur adipisicing<\/a> elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <a href=\"#\">Duis aute irure<\/a> dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia <a href=\"#\">deserunt<\/a> mollit anim id est laborum.<\/p>\n"
    },
    "excerpt": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat &hellip; <a href=\"http:\/\/local.rienkcmswp.com\/links-post\/\" class=\"more-link\"><span class=\"screen-reader-text\">&#8220;Links Post&#8221;<\/span> verder lezen<\/a><\/p>\n"
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "format": "standard",
    "categories": [1],
    "tags": [],
    "_links": {
        "self": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/13"
        }],
        "collection": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
        }],
        "about": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
        }],
        "author": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
        }],
        "replies": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=13"
        }],
        "version-history": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/13\/revisions"
        }],
        "wp:attachment": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=13"
        }],
        "wp:term": [{
            "taxonomy": "category",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=13"
        }, {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=13"
        }],
        "curies": [{
            "name": "wp",
            "href": "https:\/\/api.w.org\/{rel}",
            "templated": true
        }]
    }
}, {
    "id": 12,
    "date": "2016-08-10T04:05:26",
    "date_gmt": "2016-08-10T02:05:26",
    "guid": {
        "rendered": "http:\/\/local.rienkcmswp.com\/blockquote-post\/"
    },
    "modified": "2016-08-10T04:05:26",
    "modified_gmt": "2016-08-10T02:05:26",
    "slug": "blockquote-post",
    "type": "post",
    "link": "http:\/\/local.rienkcmswp.com\/blockquote-post\/",
    "title": {
        "rendered": "Blockquote Post"
    },
    "content": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p><\/blockquote>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n"
    },
    "excerpt": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. &hellip; <a href=\"http:\/\/local.rienkcmswp.com\/blockquote-post\/\" class=\"more-link\"><span class=\"screen-reader-text\">&#8220;Blockquote Post&#8221;<\/span> verder lezen<\/a><\/p>\n"
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "format": "standard",
    "categories": [1],
    "tags": [],
    "_links": {
        "self": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/12"
        }],
        "collection": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
        }],
        "about": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
        }],
        "author": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
        }],
        "replies": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=12"
        }],
        "version-history": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/12\/revisions"
        }],
        "wp:attachment": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=12"
        }],
        "wp:term": [{
            "taxonomy": "category",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=12"
        }, {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=12"
        }],
        "curies": [{
            "name": "wp",
            "href": "https:\/\/api.w.org\/{rel}",
            "templated": true
        }]
    }
}, {
    "id": 11,
    "date": "2016-08-10T04:05:25",
    "date_gmt": "2016-08-10T02:05:25",
    "guid": {
        "rendered": "http:\/\/local.rienkcmswp.com\/ul-and-ol-post\/"
    },
    "modified": "2016-08-10T04:05:25",
    "modified_gmt": "2016-08-10T02:05:25",
    "slug": "ul-and-ol-post",
    "type": "post",
    "link": "http:\/\/local.rienkcmswp.com\/ul-and-ol-post\/",
    "title": {
        "rendered": "UL and OL Post"
    },
    "content": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<ul>\n<li>First Item<\/li>\n<li>Second Item<\/li>\n<li>Third Item<\/li>\n<li>Fourth Item<\/li>\n<li>Fifth Item<\/li>\n<\/ul>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<ol>\n<li>First Item<\/li>\n<li>Second Item<\/li>\n<li>Third Item<\/li>\n<li>Fourth Item<\/li>\n<li>Fifth Item<\/li>\n<\/ol>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n"
    },
    "excerpt": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. First Item Second Item Third Item Fourth Item Fifth Item Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed &hellip; <a href=\"http:\/\/local.rienkcmswp.com\/ul-and-ol-post\/\" class=\"more-link\"><span class=\"screen-reader-text\">&#8220;UL and OL Post&#8221;<\/span> verder lezen<\/a><\/p>\n"
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "format": "standard",
    "categories": [1],
    "tags": [],
    "_links": {
        "self": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/11"
        }],
        "collection": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
        }],
        "about": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
        }],
        "author": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
        }],
        "replies": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=11"
        }],
        "version-history": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/11\/revisions"
        }],
        "wp:attachment": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=11"
        }],
        "wp:term": [{
            "taxonomy": "category",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=11"
        }, {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=11"
        }],
        "curies": [{
            "name": "wp",
            "href": "https:\/\/api.w.org\/{rel}",
            "templated": true
        }]
    }
}, {
    "id": 10,
    "date": "2016-08-10T04:05:25",
    "date_gmt": "2016-08-10T02:05:25",
    "guid": {
        "rendered": "http:\/\/local.rienkcmswp.com\/image-post\/"
    },
    "modified": "2016-08-10T04:05:25",
    "modified_gmt": "2016-08-10T02:05:25",
    "slug": "image-post",
    "type": "post",
    "link": "http:\/\/local.rienkcmswp.com\/image-post\/",
    "title": {
        "rendered": "Image Post"
    },
    "content": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n<p><img src=\"http:\/\/hivemindlabs.com\/plugin-data\/hive.jpg\" width=\"100%\" \/>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\/p>\n"
    },
    "excerpt": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. &hellip; <a href=\"http:\/\/local.rienkcmswp.com\/image-post\/\" class=\"more-link\"><span class=\"screen-reader-text\">&#8220;Image Post&#8221;<\/span> verder lezen<\/a><\/p>\n"
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "format": "standard",
    "categories": [1],
    "tags": [],
    "_links": {
        "self": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/10"
        }],
        "collection": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
        }],
        "about": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
        }],
        "author": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
        }],
        "replies": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=10"
        }],
        "version-history": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/10\/revisions"
        }],
        "wp:attachment": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=10"
        }],
        "wp:term": [{
            "taxonomy": "category",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=10"
        }, {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=10"
        }],
        "curies": [{
            "name": "wp",
            "href": "https:\/\/api.w.org\/{rel}",
            "templated": true
        }]
    }
}, {
    "id": 9,
    "date": "2016-08-10T04:05:24",
    "date_gmt": "2016-08-10T02:05:24",
    "guid": {
        "rendered": "http:\/\/local.rienkcmswp.com\/multiple-paragraph-post\/"
    },
    "modified": "2016-08-10T04:05:24",
    "modified_gmt": "2016-08-10T02:05:24",
    "slug": "multiple-paragraph-post",
    "type": "post",
    "link": "http:\/\/local.rienkcmswp.com\/multiple-paragraph-post\/",
    "title": {
        "rendered": "Multiple Paragraph Post"
    },
    "content": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<\/p>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<\/p>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<\/p>\n"
    },
    "excerpt": {
        "rendered": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat &hellip; <a href=\"http:\/\/local.rienkcmswp.com\/multiple-paragraph-post\/\" class=\"more-link\"><span class=\"screen-reader-text\">&#8220;Multiple Paragraph Post&#8221;<\/span> verder lezen<\/a><\/p>\n"
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "format": "standard",
    "categories": [1],
    "tags": [],
    "_links": {
        "self": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/9"
        }],
        "collection": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
        }],
        "about": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
        }],
        "author": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
        }],
        "replies": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=9"
        }],
        "version-history": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/9\/revisions"
        }],
        "wp:attachment": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=9"
        }],
        "wp:term": [{
            "taxonomy": "category",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=9"
        }, {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=9"
        }],
        "curies": [{
            "name": "wp",
            "href": "https:\/\/api.w.org\/{rel}",
            "templated": true
        }]
    }
}, {
    "id": 1,
    "date": "2016-08-10T02:05:11",
    "date_gmt": "2016-08-10T00:05:11",
    "guid": {
        "rendered": "http:\/\/local.rienkcmswp.com\/?p=1"
    },
    "modified": "2016-08-10T03:26:51",
    "modified_gmt": "2016-08-10T01:26:51",
    "slug": "hallo-wereld",
    "type": "post",
    "link": "http:\/\/local.rienkcmswp.com\/hallo-wereld\/",
    "title": {
        "rendered": "Hallo wereld."
    },
    "content": {
        "rendered": "<p>Welkom bij WordPress. Dit is je eerste bericht. Pas het aan of verwijder het en start met bloggen.<\/p>\n"
    },
    "excerpt": {
        "rendered": "<p>Welkom bij WordPress. Dit is je eerste bericht. Pas het aan of verwijder het en start met bloggen.<\/p>\n"
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "format": "standard",
    "categories": [1],
    "tags": [],
    "_links": {
        "self": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/1"
        }],
        "collection": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts"
        }],
        "about": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/types\/post"
        }],
        "author": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/users\/1"
        }],
        "replies": [{
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/comments?post=1"
        }],
        "version-history": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/posts\/1\/revisions"
        }],
        "wp:attachment": [{
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/media?parent=1"
        }],
        "wp:term": [{
            "taxonomy": "category",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/categories?post=1"
        }, {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "http:\/\/local.rienkcmswp.com\/wp-json\/wp\/v2\/tags?post=1"
        }],
        "curies": [{
            "name": "wp",
            "href": "https:\/\/api.w.org\/{rel}",
            "templated": true
        }]
    }
    }];

    }

}
