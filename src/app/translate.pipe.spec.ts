/* tslint:disable:no-unused-variable */

import { addProviders, async, inject } from '@angular/core/testing';
import { TranslatePipe } from './translate.pipe';
import { LanguageService }   from './language.service';

class MockLanguageService {};

describe('Pipe: Translate', () => {

	

  	beforeEach(() => {
	    addProviders([

	    	
	        
	        {provide: LanguageService, useClass: MockLanguageService}
	        
	    	]);
	   
	 });

  	it('should create an instance',
	    inject([LanguageService],
	     (translatePipe: TranslatePipe) => {
	      expect(translatePipe).toBeTruthy();
	    }));
 /* it('create an instance', () => {
    let pipe = new TranslatePipe();
    expect(pipe).toBeTruthy();
  });*/
});
