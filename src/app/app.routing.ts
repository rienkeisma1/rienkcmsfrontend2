import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteDispatcherComponent }    from './route-dispatcher/route-dispatcher.component';
const appRoutes: Routes = [
  { path: 'home', component: RouteDispatcherComponent },
  { path: '', component: RouteDispatcherComponent },
  { path: '_static/home', component: RouteDispatcherComponent },
  { path: '_static/', component: RouteDispatcherComponent },

  { path: '_static/detail/:id', component: RouteDispatcherComponent },
  { path: 'detail/:id', component: RouteDispatcherComponent }

  // { path: '**', component: PageNotFoundComponent }
];
export const appRoutingProviders: any[] = [
];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);