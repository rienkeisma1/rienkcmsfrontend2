import { Pipe, PipeTransform } from '@angular/core';
import { LanguageService }   from './language.service';

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

    constructor(private languageService: LanguageService) {

        this.languageService = languageService;

    }

    transform(value: any, args?: any): any {

        return this.languageService.translate(value);

    }

}
