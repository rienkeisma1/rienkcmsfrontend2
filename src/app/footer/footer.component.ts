import { Input, Component, OnInit } from '@angular/core';
import {NgFor, NgStyle} from '@angular/common';

@Component({
  selector: 'app-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['footer.component.css'],
  directives: [NgFor, NgStyle]
})
export class FooterComponent implements OnInit {

	// component to centralize the footer template
	// must be injected in a list page because position depends on list length (if list length is smaller than the height of the view the footer has)
	@Input() classes:any;
	@Input() styles:any;
    @Input() showfooter:boolean = false;
    @Input() atend: boolean = false;


  	constructor() { }

  	ngOnInit() {
  	}

}
