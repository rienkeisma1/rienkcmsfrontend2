import { Injectable } from '@angular/core';

@Injectable()
export class DimensionsService {

  	constructor() { }

  	getDimensions() {

        let w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        let h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        return {height: h, width: w};

    }


}
