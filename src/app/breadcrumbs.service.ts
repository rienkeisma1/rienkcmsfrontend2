import { Injectable } from '@angular/core';

@Injectable()
export class BreadcrumbsService {

  	constructor() { }

  	createBreadcrumbs(routedata, base) {

	  	let output = '';

	  	if(base !== null && base !== ''){

	  		output += base;

	  	}

	  	for(let i = 0; i < routedata.segments.length; i++){

	  		output +=  ' > '+routedata.segments[i];

	  	}

	  	return output;

  	}

}
