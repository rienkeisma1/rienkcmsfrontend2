/* tslint:disable:no-unused-variable */

import { addProviders, async, inject } from '@angular/core/testing';
import { DimensionsService } from './dimensions.service';

describe('Service: Dimensions', () => {
  beforeEach(() => {
    addProviders([DimensionsService]);
  });

  it('should ...',
    inject([DimensionsService],
      (service: DimensionsService) => {
        expect(service).toBeTruthy();
      }));
});
