/* tslint:disable:no-unused-variable */

import { addProviders, async, inject } from '@angular/core/testing';
import { RouteDispatcherService } from './route-dispatcher.service';

import { HttpService }              from './http.service';
import { DataAdapterService }              from './data-adapter.service';
import { LanguageService }   from './language.service';

class MockHttpService {};
class MockDataAdapterService {};
class MockLanguageService {};


describe('Service: RouteDispatcher', () => {
  beforeEach(() => {
	    addProviders([
	    	
	        {  provide: HttpService, useClass: MockHttpService},
	        {  provide: DataAdapterService, useClass: MockDataAdapterService},
	        {  provide: LanguageService, useClass: MockLanguageService}

	    	]);
	    
	 });

  it('should ...',
    inject([HttpService, DataAdapterService, LanguageService],
      (service: RouteDispatcherService) => {
        expect(service).toBeTruthy();
      }));
});
