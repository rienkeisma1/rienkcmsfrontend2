/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement, ElementRef } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { TopElementsComponent } from './top-elements.component';

import { DimensionsService } from './../dimensions.service';
import { Router }       from '@angular/router';


class MockElementRef {};
class MockDimensionsService {};



describe('Component: TopElements', () => {

	 beforeEach(() => {
	    addProviders([

	    	{provide: DimensionsService, useClass: MockDimensionsService},
	        {provide: ElementRef, useClass: MockElementRef}
	        
	    	]);
	   
	 });



	it('should create an instance', 
  	inject([DimensionsService, ElementRef] , 
  		(topElementsComponent: TopElementsComponent) => {

    //let component = new RouteDispatcherComponent();
    //let ActivatedRoute = new MockActivatedRoute();
    //let Router = new MockRouter();
    //let RouteDispatcherService = new MockRouteDispatcherService();
    //let BreadcrumbsService = new MockbreadcrumbsService();
    expect(topElementsComponent).toBeTruthy();

    }));

  /*it('should create an instance', () => {
    let component = new TopElementsComponent();
    expect(component).toBeTruthy();
  });*/
});
