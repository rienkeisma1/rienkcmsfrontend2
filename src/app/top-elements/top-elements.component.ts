import { Component, OnInit, Input, ElementRef, HostListener } from '@angular/core';
import { DimensionsService } from './../dimensions.service';

@Component({
  selector: 'app-top-elements',
  templateUrl: 'top-elements.component.html',
  styleUrls: ['top-elements.component.css']
})
export class TopElementsComponent implements OnInit {

	// creates top elements like header, breadcrumbs and pagetitle, set in config which of those are sticky or not and/or visible
	// <app-top-elements [static]='static' [pageconfig]='pageconfig' [global]='global' [breadcrumbs]='breadcrumbs' [pagetitle]='pagetitle'></app-top-elements>
    classes: any = {};
    styles: any = {};
    elementstopfixed: any = [];
    elementcontent: any = [];
    dimensions: any = {};
    ref:ElementRef;
    busy:boolean = false;

    @Input() pageconfig:any;
    @Input() global:any;
    @Input() breadcrumbs: string;
    @Input() pagetitle: string;
    @Input() headercontent: string;
    @Input() static: boolean = false;
    @Input() data: any = [];
    @Input() defaultelements: any;

    constructor(
        private dimensionsService: DimensionsService,
        ref:ElementRef

        ) {

        this.ref = ref;

    }

    ngOnInit() {

        this.dimensions = this.dimensionsService.getDimensions();
        this.setStyles();

    }

    setStyles () {
        // get breakpoint

        let brpoints = ['base', 'sm', 'md', 'lg', 'xl'];

        for (let i = 0; i < brpoints.length; i++) {

            if (this.dimensions.width >= this.global.breakpoints[brpoints[i]].minWidth) {

                this.styles.size = brpoints[i];
            } else {

                break;

            }

        }

        // top to bottom
        let topelements = ['header', 'breadcrumbs', 'pagetitle'];

        let offsetfixedtop = 0;

        let breakpoint = 0;

        // reset for resize event

        this.elementstopfixed = [];

        for (let i = 0; i < this.pageconfig.elements.length; i++) {

            if (this.pageconfig.elements[i].value === 'default' || this.pageconfig.elements[i].value === null) {

                this.pageconfig.elements[i].value = this.defaultelements[this.pageconfig.elements[i]['type']];

            }


        }

        for (let i = 0; i < this.pageconfig.elements.length; i++) {

            if (typeof this.pageconfig.elements[i].value.content !== 'undefined') {

                this.elementcontent[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.content;

            }

            // top
            if (topelements.indexOf(this.pageconfig.elements[i].type) !== -1) {

                this.classes[this.pageconfig.elements[i].type] = [];

                this.styles[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.css[this.styles.size].rules;

                if (this.pageconfig.elements[i].value.show === true) {

                    if (this.static === true) {

                        this.elementstopfixed.push({ name: this.pageconfig.elements[i].type, fixed: true, breakpoint: null, offsetfixedtop: offsetfixedtop, styles: this.styles[this.pageconfig.elements[i].type], intheight: this.styles[this.pageconfig.elements[i].type].intheight});

                        offsetfixedtop += this.styles[this.pageconfig.elements[i].type].intheight;

                    } else if (this.pageconfig.elements[i].value.isfixed === true) {

                        this.elementstopfixed.push({ name: this.pageconfig.elements[i].type, fixed: true, breakpoint: breakpoint, offsetfixedtop: offsetfixedtop, styles: this.styles[this.pageconfig.elements[i].type], intheight: this.styles[this.pageconfig.elements[i].type].intheight});

                        offsetfixedtop += this.styles[this.pageconfig.elements[i].type].intheight;

                    } else {

                        this.elementstopfixed.push({ name: this.pageconfig.elements[i].type, fixed: false, breakpoint: breakpoint, offsetfixedtop: offsetfixedtop, styles: this.styles[this.pageconfig.elements[i].type], intheight: this.styles[this.pageconfig.elements[i].type].intheight});

                        breakpoint += this.styles[this.pageconfig.elements[i].type].intheight;

                    }

                } else {

                    this.styles[this.pageconfig.elements[i].type].display = 'none';
                }

            }


            // get margin bottom
            let marginbottom = 0;

            for (let i = this.elementstopfixed.length - 1; i > -1; i--) {

                if(this.elementstopfixed[i].fixed === true){

                    marginbottom += this.elementstopfixed[i].styles.intheight;

                } else {

                    this.elementstopfixed[i].marginbottomnotfixed = marginbottom;

                    marginbottom = 0;

                }

            }

        }

        // this.offsets.top = offsettop;

        if(this.busy === false) {

            // firefox fix
            let fromtop = this.ref.nativeElement.offsetParent.scrollTop;

            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {


                fromtop = window.pageYOffset;

            }

            this.handleStickyElements(fromtop);

        }

    }


    handleStickyElements(top) {

        this.busy = true;

        let topcount = 0;

        let allfixed = true;


        for (let i = 0; i < this.elementstopfixed.length; i++) {

            if (top >= this.elementstopfixed[i].breakpoint) {

                // set fixed
                if (this.elementstopfixed[i].fixed === true) {

                    this.classes[this.elementstopfixed[i].name].fixed = true;
                    this.styles[this.elementstopfixed[i].name].top = this.elementstopfixed[i].offsetfixedtop + 'px';

                    topcount += this.styles[this.elementstopfixed[i].name].intheight;

                } else {

                    allfixed = false;

                    this.styles[this.elementstopfixed[i].name]['margin-top'] = topcount + 'px';

                    topcount = 0;

                    if (top >= this.elementstopfixed[i].breakpoint + this.elementstopfixed[i].intheight) {

                        this.styles[this.elementstopfixed[i].name]['margin-bottom'] = this.elementstopfixed[i].marginbottomnotfixed + 'px';

                    } else {

                        this.styles[this.elementstopfixed[i].name]['margin-bottom'] = 0;

                    }

                }
                
            } else {

                // unset fixed
                if(this.elementstopfixed[i].fixed === true) {

                    this.classes[this.elementstopfixed[i].name].fixed = false;

                    this.styles[this.elementstopfixed[i].name].top = 0;

                } else {

                    allfixed = false;

                    this.styles[this.elementstopfixed[i].name]['margin-top'] = topcount + 'px';

                    topcount = 0;

                    if(top === 0){

                        this.styles[this.elementstopfixed[i].name]['margin-bottom'] = 0;

                    }

                }

            }

        }

        // all elements fixed
        if (allfixed === true) {
            this.classes['topdivider'] = 'topdivider';
            this.styles['topdivider'] = {};
            this.styles['topdivider']['margin-top'] = topcount + 'px';

        }

        this.busy = false;

    }

    @HostListener('document:scroll')
        updateDisplayList() {

            if (this.busy === false && this.static === false) {

                let fromtop = this.ref.nativeElement.offsetParent.scrollTop;

                if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {

                    fromtop = window.pageYOffset;

                }

                this.handleStickyElements(fromtop);

            }

        }

    @HostListener('window:resize', ['$event'])
        onResize(event) {

        // recompute everything
        // force update rows
        this.ngOnInit();

    }

}
