import { Input, Component, OnInit, OnDestroy, OnChanges, ElementRef, HostListener, SimpleChange } from '@angular/core';
import {NgFor, NgStyle} from '@angular/common';
import { FooterComponent }         from './../footer/footer.component';

@Component({
  selector: 'app-lazy-render-page',
  templateUrl: 'lazy-render-page.component.html',
  styleUrls: ['lazy-render-page.component.css'],
  directives: [NgFor, NgStyle, FooterComponent]
})
export class LazyRenderPageComponent implements OnInit, OnDestroy, OnChanges {

	ref:ElementRef;
    
    amountrender:number = 0;

    domelements:any[] = [];

    styles:any = {};
    
    containerstyle:any = {};

    classes: any = {};

    listoffset:number = 0;

    fromtop:number = 0;

    dimensions: any;

    offsets:any = {};

    elementstopfixed: any = [];

    elementsbottomfixed: any = [];
    
    template:string;

    lazycreated:number = 0;

    lazycreatedheight:number = 0;

    computing: boolean =  false;

    startoffset: number = 0;

    atend: boolean = false;

    createtimeout: any;

    showfooter:boolean = false;

    elementcontent:any = {};

    timeout: any;

    initialized: boolean = false;

    
    @Input() pageconfig:any;
    @Input() global:any;
    @Input() static: boolean;
    @Input() pagedata: any;
    @Input() breadcrumbs: string;
    @Input() change: boolean;


  	constructor(

  		ref:ElementRef

  	) {

  		this.ref = ref;

  	}

  	ngOnInit() {

  		this.dimensions = this.getDimensions();

        this.setStyles();

        this.initialized = true;

  	}

  	ngOnChanges(changes: {[propName: string]: SimpleChange}) {
         
        // change is a boolean to trigger this change event.
        // because language is stored somewhere in an object which doesn't trigger the change event.
        // so this detects language change

        if (this.initialized === true) {

            if (this.static === true) {

                let iterations = this.pagedata.content.blocks.length;

                for (let i = 0; i < iterations; i++) {

                    this.createElement(i);
                    
                }

            } else {

                // guards agains thundering herd

                if(this.computing === false) {

                    // set lazycreated and lazycreatedheight to 0;
                    // this will force refresh of every domelement   
                    this.lazycreated = 0;
                    this.lazycreatedheight = 0;

                    this.createElementRecursive(true);

                }
           
            }

        }
    
    }

    ngOnDestroy() { 

        // clearTimeout
        window.clearTimeout(this.createtimeout);
        window.clearTimeout(this.timeout);

    }

    setStyles () {

      

        let brpoints = ['base', 'sm', 'md', 'lg', 'xl'];

        for (let i = 0; i < brpoints.length; i++) {

            if (this.dimensions.width >= this.global.breakpoints[brpoints[i]].minWidth) {

                this.styles.size = brpoints[i];

            } else {

                break;
            }

        }

        // top to bottom
        let topelements = ['header', 'breadcrumbs', 'pagetitle'];

        let offsettop = 0;

        let bottomelements = ['footer'];

        let offsetbottom = 0;

        let showfooter = false;
       
        let offsetfixedtop = 0;

        let offsetfixedbottom = 0;
       
        let fixedfooter = false;

        // set to 0 for resize
        this.startoffset = 0;

        for (let i = 0; i < this.pageconfig.elements.length; i++) {

            this.classes[this.pageconfig.elements[i].type] = [];
            this.offsets[this.pageconfig.elements[i].type] = {};
            this.elementcontent[this.pageconfig.elements[i].type] = '';

            if (typeof this.pageconfig.elements[i].value.content !== 'undefined') {
            
                this.elementcontent[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.content;

            }

            // top
            if (topelements.indexOf(this.pageconfig.elements[i].type) !== -1) {

                this.styles[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.css[this.styles.size].rules;

                if ( this.pageconfig.elements[i].value.show === true) {

                    this.startoffset += this.styles[this.pageconfig.elements[i].type].intheight;

                    if (this.pageconfig.elements[i].value.isfixed === true) {

                        this.elementstopfixed.push({ name: this.pageconfig.elements[i].type, fixed: true, breakpoint: offsetfixedtop});

                        this.offsets[this.pageconfig.elements[i].type].top = offsetfixedtop;

                        offsetfixedtop += this.styles[this.pageconfig.elements[i].type].intheight;

                    } else {

                        this.elementstopfixed.push({ name: this.pageconfig.elements[i].type, fixed: false, breakpoint: offsetfixedtop});
                    }

                    offsettop += this.styles[this.pageconfig.elements[i].type].intheight;

                } else {

                    this.styles[this.pageconfig.elements[i].type].display = 'none';
                }

            // bottom for now only one element (footer)
            } else if (bottomelements.indexOf(this.pageconfig.elements[i].type) !== -1) {

                this.styles[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.css[this.styles.size].rules;

                if (this.pageconfig.elements[i].value.show === true) {

                    showfooter = true;

                    this.showfooter = true;

                    if (this.pageconfig.elements[i].value.isfixed === true) {

                        this.elementsbottomfixed.push({ name: this.pageconfig.elements[i].type, fixed: true, breakpoint: offsetfixedbottom/*breakpoint not necessary for one element*/});
                        this.classes.footer.fixed = 'true';
                        this.styles.footer.bottom = 0;
                        this.offsets[this.pageconfig.elements[i].type].top = this.dimensions.height - this.styles[this.pageconfig.elements[i].type].intheight;
                        fixedfooter = true;

                    } else {

                        // hide on init to prevent a flashing footer
                        this.styles['footer'].visibility = 'hidden';

                        this.elementsbottomfixed.push({ name: this.pageconfig.elements[i].type, fixed: false, breakpoint: offsetfixedbottom /* breakpoint not necessary for one element */ });

                        offsetbottom += this.styles[this.pageconfig.elements[i].type].intheight;

                        this.atend = true;

                    }

                } else {

                    this.styles[this.pageconfig.elements[i].type].display = 'none';
                }

            } else if (this.pageconfig.elements[i].type === 'contentblock') {

                this.styles[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.css[this.styles.size].rules;

            }

        }

        this.offsets.top = offsettop;
        this.offsets.bottom = offsetbottom;

        // containerstyle

        if (showfooter === true) {

            this.styles.footer.right = 0;

            if (fixedfooter === true) {

                this.classes.footer.fixed = true;
                this.styles.footer.bottom = 0;

            }

        } 

        this.styles.containerstyle = {

            height: 'auto',
            width: '100%'

        };

        this.template = this.pageconfig.template;

        if ( this.static === true ) {

            this.createElements(false);

        } else {

            this.updateDisplayList();

        }

    }

    getDimensions() {

        let w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        let h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        return {height: h, width: w};

    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {

        // recompute heights

        this.ngOnInit();

        let elemlist = this.ref.nativeElement.querySelector('.homelist').querySelectorAll('.homelistrow');

        this.lazycreatedheight = 0;

        for ( let i = 0; i < elemlist.length; i++ ) {

            let curheight = elemlist[i].offsetHeight;
            this.lazycreatedheight += curheight;

        }

        let height = (this.pagedata.content.blocks.length * (this.lazycreatedheight/this.lazycreated));

        this.styles.containerstyle.height = height+'px';
        this.styles.containerstyle.intheight = height;

    }


	@HostListener('document:scroll')
    updateDisplayList() {

        this.fromtop = this.ref.nativeElement.offsetParent.scrollTop;

        if ( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ) {

            this.fromtop = window.pageYOffset;

        }


        // append one by one till it is on the end of the view height.

        if ( this.static === true ) {

            return;

        } else {

            this.createElements(true);

        }

    }

    createElements(islazy) {

        if (islazy === false) {

            let iterations = this.pagedata.content.blocks.length;

            for (let i = 0; i < iterations; i++) {

                this.createElement(i);

            }

        } else {

            // guards agains thundering herd

            if (this.computing === false) {

                this.createElementRecursive();

            }

        }

    }

    // scroll fires this a lot so guard using next in line index
    // forceupdate is for language change

    createElementRecursive(createmore?) {

        this.computing = true;

        if (typeof createmore === 'undefined') {

            createmore = this.checkCreateMore();

        }

        if (createmore === true && this.lazycreated < this.pagedata.content.blocks.length) {

            let created = this.createElement(this.lazycreated);

            if (created.id !== false) {

                this.lazycreated++;

                this.createtimeout = window.setTimeout(() => {

                    let curheight = this.ref.nativeElement.querySelector('#'+created.id).offsetHeight;

                    this.lazycreatedheight += curheight;

                    let cheight = (this.pagedata.content.blocks.length * (this.lazycreatedheight/this.lazycreated));

                    this.styles.containerstyle.height = cheight +'px';
                    this.styles.containerstyle.intheight = cheight;

                    let  cm = this.checkCreateMore();

                    if (cm === true) {

                        this.createElementRecursive(cm);

                    } else {

                        this.updateFooter();

                        this.computing = false;

                    }

                });

            }

        } else {

            this.updateFooter();

            this.computing = false;

        }

    }

    updateFooter() {

        if (this.styles['footer'].visibility === 'hidden' && this.showfooter === true) {

            this.styles['footer'].visibility = 'visible';

        }

        if (document.body.scrollHeight >= this.dimensions.height) {

            // this generates an error TODO investigate
            /* if(this.classes.footer.fixed !== true && this.showfooter === true){
                //this.classes.footer['top'] = this.pagedata.content.length * this.styles.contentrow.intheight + 'px';
            }else */ if (typeof this.classes.footer.fixed === 'undefined' || this.classes.footer.fixed === true) {

                // only if scrollbar is present or allmost to bottom
                // timout to get element height
                this.timeout = window.setTimeout(() => {

                let fromtop = this.ref.nativeElement.querySelector('.homelist').getBoundingClientRect().top;

                    if (document.body.scrollHeight > this.dimensions.height || fromtop + this.styles.contentrow.intheight + this.styles.footer.intheight > this.dimensions.height) {

                        this.atend = true;

                    } else {

                        this.atend = false;
                    }

                });

            }

        } else {

            this.classes.footer.fixed = true;
            // to create offset bottom
            this.atend = true;

        }

    }


    createElement (i) {

        if (i < this.pagedata.content.blocks.length) {

            let stylecopy = Object.assign({}, this.styles.contentblock);

            this.domelements[i] = {};

            this.domelements[i].styles = stylecopy;

            this.domelements[i].styles.top = ((i) * stylecopy.intheight)  + 'px';

            if (i === this.pagedata.content.blocks.length - 1) {

                if (this.classes.footer.fixed === true || this.classes.footer.fixed === 'true') {

                    this.classes.footer.fixed = true;

                } else {

                    this.classes.footer.fixed = false;

                }

            }

            this.domelements[i].values = this.pagedata.content.blocks[i];
            this.domelements[i].index = i;
            this.domelements[i].domid = 'lazyelement'+i;

            if (this.static !== true) {

                return {id: this.domelements[i].domid, index: i};

            }


        } else {

            if (this.static !== true) {

                return {id: false, index: i}; 

            }

        }

    }

    
    checkCreateMore() {

        if (this.lazycreated < this.pagedata.content.blocks.length && this.fromtop + this.dimensions.height - this.listoffset  >= this.lazycreatedheight ) {

            return true;

        } else {

            return false;

        }

    }


}
