/* tslint:disable:no-unused-variable */

import { By } from '@angular/platform-browser';
import { DebugElement, ElementRef } from '@angular/core';

import { addProviders, async, inject } from '@angular/core/testing';
import { LazyRenderPageComponent } from './lazy-render-page.component';

describe('Component: LazyRenderPage', () => {

	let ref:ElementRef;

	beforeEach(() => {
    	
    	
        ref = {
            nativeElement: {
                scrollTop: 1500,
                clientHeight: 600
            }
        };

    });
  it('should create an instance', () => {
    let component = new LazyRenderPageComponent(ref);
    expect(component).toBeTruthy();
  });
});
