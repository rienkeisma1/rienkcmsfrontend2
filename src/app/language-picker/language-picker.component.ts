import { Component, OnInit, OnDestroy } from '@angular/core';
import { LanguageService }   from './../language.service';
import { NgForm }    from '@angular/forms';

@Component({
  selector: 'app-language-picker',
  templateUrl: 'language-picker.component.html',
  styleUrls: ['language-picker.component.css']
})
export class LanguagePickerComponent implements OnInit, OnDestroy {
    
	public lang: any;
	public rememberlang: boolean = true;
	public languages: any;
	public selectedLang: any;
	public initlang: number;
  	languagesubscription: any; 
	private langmenu_active: boolean = false;
    initset: boolean = false;


  	constructor(private languageService: LanguageService) { }

  	ngOnInit() {

  		this.languages = this.languageService.getLanguages();

      	this.languagesubscription = this.languageService.langstream$
        .subscribe(lang => {

            // this is weird but makes it work

            if(typeof this.lang !== 'undefined'){

                this.lang = lang.lang;

                if(this.initset === false){

                    this.selectedLang = this.languages[this.lang.key];
                    this.initset = true;
                    
                }
          
            }else{

                this.lang = lang.lang;

            }  

        });

        this.rememberlang = this.languageService.getRememberLang();

  	}

  	ngOnDestroy() { 

       this.languagesubscription.unsubscribe();

    }

    changeLang(newlang) {

        this.languageService.setLang(newlang);

    }

    onChange (newlang) {

  	    this.changeLang(newlang);
        this.toggleLangMenu();
    }

    onRememberChange (rem) {
  	
  	    this.rememberlang = rem;
  	    this.languageService.setRememberLang(this.rememberlang);

    }

    toggleLangMenu() {

        this.langmenu_active = !this.langmenu_active;

    }

}
