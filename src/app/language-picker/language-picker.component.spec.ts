/* tslint:disable:no-unused-variable */
import { LanguageService }   from './../language.service';

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { LanguagePickerComponent } from './language-picker.component';

describe('Component: LanguagePicker', () => {

	beforeEach(function() {

    this.languageService = new LanguageService();

});


  it('should create an instance', () => {
    let component = new LanguagePickerComponent(this.languageService);
    expect(component).toBeTruthy();
  });
});
