import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import {BehaviorSubject} from "rxjs/BehaviorSubject"

@Injectable()
export class LanguageService {

  private rememberlang:boolean = false;
    public languages: Array<any> = window['config']['app']['languages']; // [{val: 'EN', text: 'english'}, {val:'NL', text: 'Dutch'}]
    
    public defaultlanguage =  window['config']['app']['defaultlanguage'];
    public alltranslations = window['translations'];
    public translations: any = window['translations'][this.defaultlanguage];
    public defaultlang = 0;
    // set keys
    // public for testing
	public lang = new BehaviorSubject<any>({lang: this.languages[this.defaultlang], remember: this.rememberlang, translations: this.translations });//(this.languages[this.defaultlang]);//new Subject<any>();
  

    langstream$ = this.lang.asObservable();
    private langval:any;

  	constructor() { 

  	    for(let i = 0; i < this.languages.length; i++){

            this.languages[i].key = i;

            if(this.languages[i].val == this.defaultlanguage){

                this.defaultlang = i;
            }

        }

  		this.langval = this.languages[this.defaultlang];

  	}

  	public getLang() {

  	    return this.langval.val;

    }

    public setLang(l) {

        this.langval = this.languages[l.key];

        this.streamLang();
	
    }

    public getRememberLang() {

        return this.rememberlang;

    }

    public setRememberLang(val:boolean) {

        this.rememberlang = val;

        this.streamLang();

    }


    public streamLang() {

        this.lang.next({lang: this.langval, remember: this.rememberlang, translations: this.alltranslations[this.langval.val]});

    }

    public getLanguages() {

        return this.languages;

    }

    public translate(translationkey) {

        if(typeof this.alltranslations === 'undefined'){

            return translationkey;

        }

        else if(typeof this.alltranslations[this.langval.val] === 'undefined'){

            return translationkey;

        }

        else if (typeof this.alltranslations[this.langval.val][translationkey] === 'undefined'){

            return translationkey;

        }

        else {

            return this.alltranslations[this.langval.val][translationkey];

        }
        
    }

}
