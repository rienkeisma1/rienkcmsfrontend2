import { Injectable, OnDestroy } from '@angular/core';
// import { Router, ActivatedRoute }   from '@angular/router';
import { Observable }               from 'rxjs/Observable';
import { HttpService }              from './http.service';
import { DataAdapterService }              from './data-adapter.service';
import { Subject }    from 'rxjs/Subject';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { LanguageService } from './language.service';


@Injectable()
export class RouteDispatcherService  implements OnDestroy {

    globalconfig:any;
    global:any;
    reqroute:any = {};
    pageconfig:any = {};
    pagetype: string = '';
    configset: boolean = false;
    static:boolean = false;
    data: Observable<any[]>;
    pagedata: any = {};
    cacheddata: any = {};
    cacheamount: number = 10;
    calls: number = 0;
    defaultelements: any;
    langval:string;
    languagesubscription: any;
    httpsubscription: any;
    datasubscription: any;

    private pdata = new BehaviorSubject<any>({}); // new Subject<any>();
    datastream$ = this.pdata.asObservable();

  	constructor(

  		// private router: Router,
        private httpService: HttpService,
        private dataAdapterService: DataAdapterService,
        private languageService: LanguageService

  	) { 

        console.log('komt in routedispatcher');

        this.languagesubscription = this.languageService.langstream$
        .subscribe(lang => {

            this.langval = lang.lang.val.toLowerCase();
            // fetch data on change
            if(typeof this.pagedata.loaded !== 'undefined'){

                if(this.pagedata.loaded === true){

                    // re-fetch data with new language data will be streamed
                    this.getData();

                }

            }
            
        })  

  	}

  	ngOnDestroy() { 
        
        this.languagesubscription.unsubscribe();
        this.httpsubscription.unsubscribe();
        this.datasubscription.unsubscribe();

    }

    getPagedata(reqroute){

    	this.reqroute = reqroute;

    	this.getConfig();

    }

    getConfig() {

        if( this.configset === false){

            this.httpsubscription = this.httpService.getConfig().subscribe(
                res => { 

                    this.defaultelements = res.app.pages.defaultelements;

                    if(window['configsettings']['datakey'].length !== 0){

                        let datakeys = window['configsettings']['datakey'];

                        let content = res;

                        for (var i = 0; i < datakeys.length; i++){

                            content = content[datakeys[i]];

                        }

                        this.globalconfig = content;

                    }else{

                        this.globalconfig = res;

                    }

                    // get defaultlanguage

                    let lang = {};

                    for(let i = 0; i < this.globalconfig['app']['languages'].length; i++){

                        if(this.globalconfig['app']['languages'][i].val === this.globalconfig['app']['defaultlanguage']){

                            lang = this.globalconfig['app']['languages'][i];
                            break;
                        }

                    }

                    this.languageService.setLang(lang);

                    this.configset = true;
       
                    this.dispatchRoute();
                  
                },

                err => {

                    // TODO handle error
                    console.log(err);

                }

            );

        }else{

            this.dispatchRoute();

        }

    }


    getData() {

        if(this.pageconfig.api.hasdata === true){

        	let url = this.pageconfig.api.url;

            // replace {language} with langval
            url = url.replace('{language}', this.langval);

            url = url.replace('{slug}', this.reqroute.params.id);

        	// if(typeof this.reqroute.params.id !== 'undefined'){

                // url += '/'+this.reqroute.params.id;

            // }

            // check cached

            // this crashes after a few navigations... TODO research (maybe some reference gets orphaned)
            // if(typeof this.cacheddata[url] === 'undefined' || this.calls > this.cacheamount){
            if(true === true){
            
                // ? unsubscribe before new call?
	            this.datasubscription = this.httpService.call(url, this.pageconfig.api.type, this.globalconfig).subscribe(

	                res => {

	                	this.calls++;

                    let adapteddata = this.dataAdapterService.adaptData(res, this.globalconfig.app, this.reqroute);

	                    this.pagedata.pagetitle = adapteddata['pagetitle'];

	                    this.pagedata.content = adapteddata;

	                    this.pagedata.loaded = true;

	                    /* if(this.calls > this.cacheamount){
	                    	this.cacheddata = {};
	                    	this.calls = 0;
	                    }
	                    
	                    this.cacheddata[url] =  Object.assign({}, this.pagedata); */
	                   
	                    this.streamData();
	                    
	                },

	                err => {

	                    // TODO handle error
	                    console.log(err);

	                }

	            );

	        }else{

	        	// this.pagedata = this.cacheddata[url];

	        	this.streamData();

	        }

        }else{

            this.pagedata = this.pageconfig.defaultdata;
            this.pagedata.loaded = true;
            this.streamData();

        }

    }

    streamData() {

    	this.pdata.next({pagedata: this.pagedata, static: this.static, pageconfig: this.pageconfig, reqroute: this.reqroute, pagetype:  this.pagetype, global: this.global, defaultelements:this.defaultelements});

  	}

  	dispatchRoute () {

     // get config
     this.getPageConfig();

    }

    getPageConfig () {

        // match correct route
        // for now only one param can be matched
        let route = this.reqroute.route;
        let matched = false;

        if(typeof this.globalconfig.app.pages[route] !== 'undefined'){

            if(typeof this.globalconfig.app.pages[route].redirect !== 'undefined'){

                route = this.globalconfig.app.pages[route].redirect;
               
                this.reqroute.route = route;

            }

            this.pageconfig = this.globalconfig.app.pages[route];

            matched = true;
            this.pagetype = this.pageconfig.type;
            this.global = {

                title: this.globalconfig.app.title, 
                breadcrumbbase: this.globalconfig.app.breadcrumbbase, 
                breakpoints: this.globalconfig.app.breakpoints,
                contentwidth: this.globalconfig.app.contentwidth

            }

            this.pageconfig.loaded = true;
            this.getData();
            
        }else{

            // check for dynamic route
            if(this.reqroute.segments.length > 1){

                let pos = route.lastIndexOf('/');
                route = route.substr(0,pos);

                if(typeof this.globalconfig.app.pages[route+'/:param'] !== 'undefined'){

                    this.pageconfig = this.globalconfig.app.pages[route+'/:param'];

                    matched = true;
                    this.pagetype = this.pageconfig.type;
                    this.global = {

                        title: this.globalconfig.app.title, 
                        breadcrumbbase: this.globalconfig.app.breadcrumbbase, 
                        breakpoints: this.globalconfig.app.breakpoints,
                        contentwidth: this.globalconfig.app.contentwidth

                    }

                    this.pageconfig.loaded = true;
                    this.getData();

                }

            }
            
        }

        document.title = this.globalconfig.app.title;

        if(matched = false){

             // page not found
             // router can't be injected
             // TODO create redirect through service or something which does not create an error on router injection
             // this.router.navigateByUrl('**');

        }
 
    }

}
