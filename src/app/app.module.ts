import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LanguageService } from './language.service';
import { LanguagePickerComponent } from './language-picker/language-picker.component';
import { routing, appRoutingProviders } from './app.routing';
import { TranslatePipe } from './translate.pipe';
import { RouteDispatcherComponent } from './route-dispatcher/route-dispatcher.component';
import { RouteDispatcherService } from './route-dispatcher.service';
import { HttpService } from './http.service';
import { HTTP_PROVIDERS } from '@angular/http';
import { DataAdapterService } from './data-adapter.service';
import { BreadcrumbsService } from './breadcrumbs.service';
import { VirtualListComponent } from './virtual-list/virtual-list.component';DimensionsService
import { DimensionsService } from './dimensions.service';
import { FooterComponent } from './footer/footer.component';
import { TopElementsComponent } from './top-elements/top-elements.component';
import { LazyRenderPageComponent } from './lazy-render-page/lazy-render-page.component';


@NgModule({
  declarations: [
    AppComponent,
    LanguagePickerComponent,
    TranslatePipe,
    RouteDispatcherComponent,
    VirtualListComponent,
    FooterComponent,
    TopElementsComponent,
    LazyRenderPageComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    routing
  ],
  providers: [LanguageService, appRoutingProviders, HTTP_PROVIDERS, HttpService, DataAdapterService, BreadcrumbsService, DimensionsService, RouteDispatcherService],
  entryComponents: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {

}
