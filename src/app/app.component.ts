import { Component, OnInit, OnDestroy } from '@angular/core';
import { LanguageService } from './language.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  	

  	menu_active: boolean = false;
    lang: any;
    pagedata: any;
    languagesubscription: any;

    constructor(public languageService: LanguageService) {

        console.log('appcomponent loaded');
    }

    ngOnInit() {

        this.languagesubscription = this.languageService.langstream$.subscribe(lang => { console.log(lang); this.lang = lang;});

    }

    ngOnDestroy() { 
        
        this.languagesubscription.unsubscribe();
        
    }

    toggleMenu() {

        this.menu_active = !this.menu_active;
    
    }

}

