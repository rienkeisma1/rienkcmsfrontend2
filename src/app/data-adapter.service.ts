import { Injectable } from '@angular/core';

@Injectable()
export class DataAdapterService {

  	constructor() { }

  	adaptData(data, config, route){

	  	// TODO error handling
	  	
	  	if(typeof config.pages[route['route']] !== 'undefined'){

	  		if(config.pages[route['route']]['api']['multiple'] === false){

	  			let rdata = this.handleData(data, config, route);

	  			return rdata;

	  		}else{

	  			// multiple
	  			let returndata = [];

	  			for(let i = 0; i < data.length; i++){

	  				if(i===3){

	  					break;
	  				}

	  				returndata.push(this.handleData(data[i], config, route));

	  			}

	  			for(let i = 0; i < data.length; i++){

	  				returndata.push(this.handleData(data[i], config, route));

	  			}
	  			for(let i = 0; i < data.length; i++){

	  				returndata.push(this.handleData(data[i], config, route));

	  			}
	  			for(let i = 0; i < data.length; i++){

	  				returndata.push(this.handleData(data[i], config, route));

	  			}
	  			for(let i = 0; i < data.length; i++){

	  				returndata.push(this.handleData(data[i], config, route));

	  			}
	  			for(let i = 0; i < data.length; i++){

	  				returndata.push(this.handleData(data[i], config, route));

	  			}

	  			returndata['pagetitle'] = '<p>Virtual repeat home-list, rows with same height</p>';

	  			return returndata;

	  		}
	  	
	  	}else{

			// return error route not found

		}
  		
  	}

  	handleData(data, config, route){

  		let d = {};

  		let idkeys = config.pages[route['route']]['api']['idkey'];

  		let id = data;

  		for (var i = 0; i < idkeys.length; i++){

			id = id[idkeys[i]];

		}
  	

  		let contentkeys = config.pages[route['route']]['api']['contentkey'];

  		let content = data;

  		for (var i = 0; i < contentkeys.length; i++){

			content = content[contentkeys[i]];

		}

		let titlekeys = config.pages[route['route']]['api']['titlekey'];

		let title = data;

  		for (var i = 0; i < titlekeys.length; i++){

			title = title[titlekeys[i]];

		}

		let excerptkeys = config.pages[route['route']]['api']['excerptkey'];

		let excerpt = data;

  		for (var i = 0; i < excerptkeys.length; i++){

			excerpt = excerpt[excerptkeys[i]];

		}

		let authorkeys = config.pages[route['route']]['api']['authorkey'];

		let author = data;

  		for (var i = 0; i < authorkeys.length; i++){

			author = author[authorkeys[i]];

		}

		let categorieskeys = config.pages[route['route']]['api']['categorieskey'];

		let category = data;

  		for (var i = 0; i < categorieskeys.length; i++){

			category = category[categorieskeys[i]];

		}

		let slugkeys = config.pages[route['route']]['api']['slugkey'];

		let slug = data;

  		for (var i = 0; i < slugkeys.length; i++){

			slug = slug[slugkeys[i]];

		}

		// to lazy-render maybe split it up (maybe split by <p> tags)


	  	d['blocks'] = [];
	  	d['blocks'].push({ blockContent: content.substring(0, Math.round(Math.random()* content.length)) + '//Endblock//', blockmedia: null});
	  	d['blocks'].push({ blockContent: content.substring(0, Math.round(Math.random()* content.length)) + '//Endblock//', blockmedia: null});
	  	d['blocks'].push({ blockContent: content.substring(0, Math.round(Math.random()* content.length)) + '//Endblock//', blockmedia: null});
	  	d['blocks'].push({ blockContent: content.substring(0, Math.round(Math.random()* content.length)) + '//Endblock//', blockmedia: null});
	  	d['blocks'].push({ blockContent: content.substring(0, Math.round(Math.random()* content.length)) + '//Endblock//', blockmedia: null});
	  	d['excerpt'] = excerpt;
	  	d['category'] = category;
	  	d['postAuthor'] = author;
	  	d['pagetitle'] = '<p>Different blocks with random heights/length</p>';
	  	d['id'] = id;
	  	d['slug'] = slug;

	  	return d;

  	}

}
