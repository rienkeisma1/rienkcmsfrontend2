/* tslint:disable:no-unused-variable */

import { addProviders, async, inject } from '@angular/core/testing';
import { DataAdapterService } from './data-adapter.service';

describe('Service: DataAdapter', () => {
  beforeEach(() => {
    addProviders([DataAdapterService]);
  });

  it('should ...',
    inject([DataAdapterService],
      (service: DataAdapterService) => {
        expect(service).toBeTruthy();
      }));
});
