/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement, ElementRef } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { VirtualListComponent } from './virtual-list.component';

import { DimensionsService } from './../dimensions.service';
import { Router }       from '@angular/router';

class MockElementRef {};
class MockDimensionsService {};
class MockRouter {};

describe('Component: VirtualList', () => {

	beforeEach(() => {
	    addProviders([

	    	
	        {provide: ElementRef, useClass: MockElementRef},
	        {provide: Router, useClass: MockRouter},
	        {provide: DimensionsService, useClass: MockDimensionsService},
	        
	    	]);
	   
	 });

	it('should create an instance', 
  	inject([ElementRef, Router, DimensionsService],
  	 (virtualListComponent: VirtualListComponent) => {

    //let component = new RouteDispatcherComponent();
    //let ActivatedRoute = new MockActivatedRoute();
    //let Router = new MockRouter();
    //let RouteDispatcherService = new MockRouteDispatcherService();
    //let BreadcrumbsService = new MockbreadcrumbsService();
    expect(virtualListComponent).toBeTruthy();

    }));

  /*it('should create an instance', () => {
    let component = new VirtualListComponent();
    expect(component).toBeTruthy();
  });*/
});
