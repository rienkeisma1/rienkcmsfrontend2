import { Input, Component, OnInit, OnDestroy, OnChanges, ElementRef, HostListener, SimpleChange } from '@angular/core';
import {NgFor, NgStyle} from '@angular/common';
import { DimensionsService } from './../dimensions.service';
import { Router }   from '@angular/router';
import { FooterComponent }         from './../footer/footer.component';

@Component({
  selector: 'app-virtual-list',
  templateUrl: 'virtual-list.component.html',
  styleUrls: ['virtual-list.component.css'],
  directives: [NgFor, NgStyle, FooterComponent]
})
export class VirtualListComponent implements OnInit, OnDestroy, OnChanges {

    ref:ElementRef;

    amountrender:number = 0;

    domelements:any[] = [];

    styles:any = {};

    containerstyle:any = {};

    classes: any = {};

    dimensions: any;

    elementsbottomfixed: any = [];

    atend: boolean = false;

    template:string;

    elementcontent:any = {};

    showfooter: boolean = false;

    resizeevent: boolean = false;

    updating: boolean = false;

    listdata: any = [];

    createtimeout: any;

    timeout: any;

    pagetitle:string;

    start:number;

    @Input() pageconfig:any;
    @Input() global:any;
    @Input() static: boolean;
    @Input() pagedata: any;
    @Input() breadcrumbs: string;
    @Input() change: boolean;

    constructor(

        ref:ElementRef,
        private router: Router,
        private dimensionsService: DimensionsService

        ) {

        this.ref = ref;

    }

    ngOnInit() {

        this.dimensions = this.dimensionsService.getDimensions(); // this.getDimensions();
        this.setStyles();

  	}

    ngOnDestroy() {

        // clearTimeouts
        window.clearTimeout(this.createtimeout);
        window.clearTimeout(this.timeout);

    }

    ngOnChanges(changes: {[propName: string]: SimpleChange}) {

        this.createListData(this.start,  this.amountrender);

    }

   
    toDetail(id) {


        this.router.navigate(['/detail', id]);

    }

    // also called on resize
    setStyles () {

        // get breakpoint 
        let brpoints = ['base', 'sm', 'md', 'lg', 'xl'];

        for (let i = 0; i < brpoints.length; i++) {

            if (this.dimensions.width >= this.global.breakpoints[brpoints[i]].minWidth) {

                this.styles.size = brpoints[i];

            } else {

                break;
            }

        }

        // top to bottom

        let pageelements = ['contentrow', 'footer'];

        let offsettop = 0;

        let bottomelements = ['footer'];

        let offsetbottom = 0;

        let offsetfixedbottom = 0;

        let fixedfooter = false;

        for (let i = 0; i < this.pageconfig.elements.length; i++) {

            this.classes[this.pageconfig.elements[i].type] = [];
            // this.offsets[this.pageconfig.elements[i].type] = {};

            if (typeof this.pageconfig.elements[i].value.content !== 'undefined' && pageelements.indexOf(this.pageconfig.elements[i].type) !== -1) {

                this.elementcontent[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.content;

                // bottom for now only one element (footer)
            } else if (bottomelements.indexOf(this.pageconfig.elements[i].type) !== -1) {

                this.styles[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.css[this.styles.size].rules;

                if( this.pageconfig.elements[i].value.show === true) {

                    this.showfooter = true;

                    if (this.pageconfig.elements[i].value.isfixed === true) {

                        fixedfooter = true;

                        this.elementsbottomfixed.push({ name: this.pageconfig.elements[i].type, fixed: true, breakpoint: offsetfixedbottom});
                        this.classes.footer.fixed = true;
                        this.styles.footer.bottom = 0;

                    } else {

                        this.elementsbottomfixed.push({ name: this.pageconfig.elements[i].type, fixed: false, breakpoint: offsetfixedbottom});
                        
                        offsetbottom += this.styles[this.pageconfig.elements[i].type].intheight;

                    }

                } else {

                    this.styles[this.pageconfig.elements[i].type].display = 'none';
                }

            } else if (this.pageconfig.elements[i].type === 'contentrow') {

                this.styles[this.pageconfig.elements[i].type] = this.pageconfig.elements[i].value.css[this.styles.size].rules;

                if (this.static === true) {

                    this.amountrender = this.pagedata.content.length;

                } else {

                    this.amountrender = Math.round(this.dimensions.height / this.styles[this.pageconfig.elements[i].type].intheight) * 3;

                    if (this.amountrender > this.pagedata.content.length) {

                        this.amountrender = this.pagedata.content.length;

                    }

                }

            }

        }

        // containerstyle
        let conth = this.pagedata.content.length * this.styles.contentrow.intheight;

        if (this.showfooter === true && this.classes.footer.fixed !== true) {

            conth += this.styles.footer.intheight;

        }

        if (conth + offsettop + offsetbottom < this.dimensions.height) {

            conth = 0; // this.dimensions.height - (offsettop + offsetbottom);
            if (this.showfooter === true) {

                this.styles.footer.right = 0;

                if (fixedfooter === true) {

                    this.classes.footer.fixed = true;
                    this.styles.footer.bottom = 0;

                }

            } 

        }

        this.styles.containerstyle = {
            
            height: conth + 'px',
            width: '100%'

        };

        this.template = this.pageconfig.template;

        if (this.pagedata.content.length === 0) {

            // doesnt get called when no content is present
            this.updateFooter();

        }

        if (this.static === true) {

            this.createElements(0, false);

        } else {

            this.updateDisplayList();

        }

    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {

        // force update rows
        this.resizeevent = true;

        // recompute everything
        this.ngOnInit();

    }

	@HostListener('document:scroll')
    updateDisplayList() {

        if (this.updating === false) {

            this.updating = true;

            let fromtop = this.ref.nativeElement.offsetParent.scrollTop;

            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {

                fromtop = window.pageYOffset;

            }

            if (this.static === true) {

                this.updating = false;

                return;

            } else if (this.amountrender === this.pagedata.content.length) {

                this.atend = true;
                this.createElements(0, false);
                this.updating = false;

            } else if ((this.amountrender / 3) * this.styles.contentrow.intheight > fromtop) {

                this.atend = false;
                // start from top
                this.createElements(0, false);
                this.updating = false;

            } else if (this.dimensions.height + fromtop > this.pagedata.content.length * this.styles.contentrow.intheight) {
                // start from end minus amountrender

                if (typeof this.elementsbottomfixed[0].fixed !== 'undefined' && this.elementsbottomfixed[0].fixed === false) {

                    this.atend = true;
                    this.styles.footer.position = 'relative';

                }

                this.createElements(this.pagedata.content.length - this.amountrender, false);
                this.updating = false;

            } else {

                this.atend = false;
                // render center with overlapping top and bottom

                console.log(this.pagedata.content.length);
                console.log(this.amountrender);
                let st = Math.floor(fromtop / this.styles.contentrow.intheight) - this.amountrender / 3;

                if (st > this.pagedata.content.length - this.amountrender) {

                    st  = this.pagedata.content.length - this.amountrender;
                }

                console.log(st);
                this.createElements(st, true);
                this.updating = false;

            }

        }

    }

    createElements(start, optimize) {
        console.log('creating domelements');

        this.start = start;

        let iterations = 0;

        if (this.domelements.length === 0 || this.domelements.length !== this.amountrender || this.resizeevent === true) {

            iterations = this.amountrender;
            if (this.domelements.length > this.amountrender) {

                // resize can decrease ammountrender > make sure no domelements remain
                let chop = this.domelements.length - this.amountrender;

                for (let i = 0; i < chop; i++) {

                    this.domelements.pop();

                }

            }

            this.resizeevent = false;

         // don't create if index is the same or within 1/2 of screenheight ===  1/6 of domelements
        } else if (this.domelements[0].index === start || (optimize === true && (this.domelements[0].index + this.amountrender / 6 > start && start > this.domelements[0].index - this.amountrender / 3) ) ) {

            iterations = 0;
            // resize footer get called on resize
            this.updateFooter();

        } else {

            iterations = this.domelements.length;

        }

        for (let i = 0; i < iterations; i++) {

            let stylecopy = Object.assign({}, this.styles.contentrow);

            this.domelements[i] = {};

            this.domelements[i].styles = stylecopy;

            if (i === 0) {

                this.domelements[i].styles['margin-top'] = start * stylecopy.intheight + 'px';

            }

            if (start + i === this.pagedata.content.length - 1) {

                if (this.showfooter === true) {

                    this.domelements[i].styles['margin-bottom'] = this.classes.footer.intheight + 'px';

                    this.updateFooter();

                }

            }



            this.domelements[i].index = start + i;

        }

        // after creating domelements create data variable with corresponding keys so it will not block domelement creation
        // timeout to give rendering of containers precedence
        this.createtimeout = window.setTimeout(() => {

            if(iterations !== 0) {

                this.createListData(start, iterations);

            }

        });

    }

    createListData (start, iterations) {

        console.log('creating listdata');

        for(let i = 0; i < iterations; i++) {

            this.listdata[start + i] = this.pagedata.content[start + i];
            this.listdata[start + i]['index'] = start + i;

        }

        // clean up
        for(let key in this.listdata) {

            if (key > start + iterations || key < start ) {

                delete this.listdata[key];

            }

        }

    }

    updateFooter() {

        if (this.styles['footer'].visibility === 'hidden' && this.showfooter === true) {

            this.styles['footer'].visibility = 'visible';

        }

        if (document.body.scrollHeight >= this.dimensions.height) {

            if (this.classes.footer.fixed !== true && this.showfooter === true) {


                this.classes.footer['top'] = this.pagedata.content.length * this.styles.contentrow.intheight + 'px';

            } else if (typeof this.classes.footer.fixed === 'undefined' || this.classes.footer.fixed === true) {
                // only if scrollbar is present or allmost to bottom
                // timout to get element height
                this.timeout = window.setTimeout(() => {

                    let fromtop = this.ref.nativeElement.querySelector('.homelist').getBoundingClientRect().top;

                    if (document.body.scrollHeight > this.dimensions.height || fromtop + this.styles.contentrow.intheight + this.styles.footer.intheight > this.dimensions.height) {

                        this.atend = true;

                    } else {

                        this.atend = false;
                    }

                });

            }

        } else {

            this.classes.footer.fixed = true;
            // to create offset bottom
            this.atend = true;

        }    

    }

}
