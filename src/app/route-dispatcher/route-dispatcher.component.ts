import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute }       from '@angular/router';
import { RouteDispatcherService }       from './../route-dispatcher.service';
import { BreadcrumbsService }           from './../breadcrumbs.service';



@Component({
  selector: 'app-route-dispatcher',
  templateUrl: 'route-dispatcher.component.html',
  styleUrls: ['route-dispatcher.component.css'] // ,
  // providers: [RouteDispatcherService]
})
export class RouteDispatcherComponent implements OnInit {
    global:any;
    reqroute:any = {};
    pageconfig:any = {};
    pagetype: string = '';
    static:boolean = false; 
    pagedata: any = {};
    breadcrumbs: string;
    pagetitle: string = '';
    // routeDispatcherService: any;
    subscription: any;
    routesubscription: any;
    headercontent: string;
    defaultelements: any;
    change: boolean = false;

    constructor(

  		private route: ActivatedRoute,
    	private router: Router,
        private routeDispatcherService: RouteDispatcherService,
        private breadcrumbsService: BreadcrumbsService

  		) {

        console.log('hit');

   	}

  	ngOnInit() {

  		this.subscription = this.routeDispatcherService.datastream$.subscribe( res => {

            if (Object.keys(res).length === 0 && res.constructor === Object) {

            // do nothing
                
            } else {

                this.reqroute = res['reqroute'];
                this.pagetype = res['pagetype'];
                this.global = res['global'];
                this.defaultelements = res['defaultelements'];
                // assign last to prevent above undefined
                this.pagedata = res['pagedata'];
                this.pageconfig = res['pageconfig'];
                
                if(typeof res['pagedata']!== 'undefined' && typeof res['pagedata']['pagetitle'] !== 'undefined') {

                    this.pagetitle = res['pagedata']['pagetitle'];

                }       
       
                if(typeof this.global !== 'undefined'){

                    this.breadcrumbs = this.breadcrumbsService.createBreadcrumbs(this.reqroute, this.global.breadcrumbbase);

                }

                // trigger change detection
                this.change = !this.change;

            }

        });

        this.Init();

    }

    ngOnDestroy() { 
        
        // unsubscribe to prevent memory leaks and else next instance will be getting destroyed instances subscriptions
        this.subscription.unsubscribe();
        this.routesubscription.unsubscribe();

    }

    Init() {

        this.reqroute = {};

        this.reqroute.route = this.router.url;

        let r = this.reqroute.route;

        if (r[0] === '/') {

            r = this.reqroute.route.slice(1);

        }

        this.reqroute.segments = r.split('/');

        if (this.reqroute.segments[0] === '_static') {

            this.static = true;
            this.reqroute.segments.shift();
            this.reqroute.route = this.reqroute.route.slice(('/_static').length);

        }

        this.routesubscription = this.route.params
  		    .subscribe(params => {

            this.reqroute.params = params;

            let parlength = Object.keys(params).length;

            if (parlength === 0) {

                this.routeDispatcherService.getPagedata(this.reqroute);

            } else {

                for (let i = 0; i < parlength; i++) {

                    this.reqroute.route = this.reqroute.route.substring(0, this.reqroute.route.lastIndexOf('/'));

                }

                this.routeDispatcherService.getPagedata(this.reqroute);

            }

        });

    }

}
