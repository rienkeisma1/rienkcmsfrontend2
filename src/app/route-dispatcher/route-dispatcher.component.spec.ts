/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { RouteDispatcherComponent } from './route-dispatcher.component';
import { Router, ActivatedRoute }       from '@angular/router';
import { RouteDispatcherService }       from './../route-dispatcher.service';
import { BreadcrumbsService }           from './../breadcrumbs.service';

// import {ROUTER_PRIMARY_COMPONENT} from 'angular2/router';

class MockActivatedRoute {};
class MockRouter {};
class MockRouteDispatcherService{};
class MockbreadcrumbsService{};

describe('Component: RouteDispatcher', () => {

	 beforeEach(() => {
	    addProviders([
	    	{provide: ActivatedRoute, useClass: MockActivatedRoute},
	        {provide: Router, useClass: MockRouter},
	        {provide: RouteDispatcherService, useClass: MockRouteDispatcherService},
	        {provide: BreadcrumbsService, useClass: MockbreadcrumbsService}

	    	]);
	    //this.languageService = new LanguageService();
	 });

	
  it('should create an instance', 
  	inject([ActivatedRoute, Router, RouteDispatcherService, BreadcrumbsService], 
  		(routeDispatcherComponent: RouteDispatcherComponent) => {

    //let component = new RouteDispatcherComponent();
    //let ActivatedRoute = new MockActivatedRoute();
    //let Router = new MockRouter();
    //let RouteDispatcherService = new MockRouteDispatcherService();
    //let BreadcrumbsService = new MockbreadcrumbsService();
    expect(routeDispatcherComponent).toBeTruthy();

    }));





});
