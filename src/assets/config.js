


window.configsettings = {

    "configfromserver": false,
    "url": null, //url to get config from server
    "type": null, // get, post, etc
    "datakey":[] //if config get returns in a datakey(s): for instance result is in 'response.data' insert ['response', 'data']
}




var configwordpress = {
    "loadconfigjs": true,
    "dummy": false,
    "app": {
    	languages: [

        {val: 'EN', translationkey: 'english'}, 

        {val:'NL', translationkey: 'dutch'}

        ],

        "defaultlanguage": 'NL',
        "title": "rienkCMS",
        "breadcrumbbase": "rienkCMS",
        "apitype": "dummyapi",
        "breakpoints": {
            "base": {
                "minWidth": 0
            },
            "sm": {
                "minWidth": 568
            },
            "md": {
                "minWidth": 768
            },
            "lg": {
                "minWidth": 1024
            },
            "xl": {
                "minWidth": 1280
            }
        },
        "contentwidth": {
            "base": {
                "width": null
            },
            "sm": {
                "width": null
            },
            "md": {
                "width": null
            },
            "lg": {
                "width": 768
            },
            "xl": {
                "width": 1024
            }
        },

        //for page elemaents use values 'default' or null if you want to use the default values (below in defaultelements)

        "pages": {

            "defaultelements" : {

                
                "header": {
                    "content":"homeheader",
                    "show": true,
                    "isfixed": false,
                    "css": {
                        "base": {
                            "rules": {
                                "intheight": 100,
                                "height": "100px",
                                "background-color": "rgba(193, 176, 176, 0.95)", 
                                "z-index": 30,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "sm": {
                            "rules": {
                                "intheight": 90,
                                "height": "90px",
                                "background-color":"rgba(193, 176, 176, 0.95)",
                                "z-index": 30,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "md": {
                            "rules": {
                                "intheight": 80,
                                "height": "80px",
                                "background-color":"rgba(193, 176, 176, 0.95)",
                                "z-index": 30,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "lg": {
                            "rules": {
                                "intheight": 70,
                                "height": "70px",
                                "background-color": "rgba(193, 176, 176, 0.95)",
                                "z-index": 30,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "xl": {
                            "rules": {
                                "intheight": 60,
                                "height": "60px",
                                "background-color": "rgba(193, 176, 176, 0.95)",
                                "z-index": 30,
                                "padding": 0,
                                "margin": 0
                            }
                        }
                    }
                },
            
                
                "breadcrumbs": {
                    "show": true,
                    "isfixed": true,
                    "css": {
                        "base": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "grey",
                                "z-index": 20,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "sm": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "grey",
                                "z-index": 20,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "md": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "grey",
                                "z-index": 20,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "lg": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "grey",
                                "z-index": 20,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "xl": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "grey",
                                "z-index": 20,
                                "padding": 0,
                                "margin": 0
                            }
                        }
                    }
                },
            
               
                "pagetitle": {
                    "show": true,
                    "isfixed": false,
                    "css": {
                        "base": {
                            "rules": {
                                "intheight": 70,
                                "height": "70px",
                                "background-color": "rgba(150, 133, 133, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0

                            }
                        },
                        "sm": {
                            "rules": {
                                "intheight": 60,
                                "height": "60px",
                                "background-color": "rgba(150, 133, 133, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "md": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "rgba(150, 133, 133, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "lg": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "rgba(150, 133, 133, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "xl": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "rgba(150, 133, 133, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        }
                    }
                },
            
               
                "contentrow": {
                    "show": true,
                    "isFixed": false,
                    "css": {
                        "base": {
                            "rules": {
                                "intheight": 200,
                                "height": "200px"
                            }
                        },
                        "sm": {
                            "rules": {
                                "intheight": 160,
                                "height": "160px"
                            }
                        },
                        "md": {
                            "rules": {
                                "intheight": 140,
                                "height": "140px"
                            }
                        },
                        "lg": {
                            "rules": {
                                "intheight": 120,
                                "height": "120px"
                            }
                        },
                        "xl": {
                            "rules": {
                                "intheight": 100,
                                "height": "100px"
                            }
                        }
                    }
                },
            
               
                "footer": {
                    "show": true,
                    "isfixed": true,
                    "css": {
                        "base": {
                            "rules": {
                                "intheight": 40,
                                "height": "40px",
                                "background-color": "rgba(162, 135, 135, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "sm": {
                            "rules": {
                                "intheight": 45,
                                "height": "45px",
                                "background-color": "rgba(162, 135, 135, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "md": {
                            "rules": {
                                "intheight": 50,
                                "height": "50px",
                                "background-color": "rgba(162, 135, 135, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "lg": {
                            "rules": {
                                "intheight": 60,
                                "height": "60px",
                                "background-color": "rgba(162, 135, 135, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        },
                        "xl": {
                            "rules": {
                                "intheight": 70,
                                "height": "70px",
                                "background-color": "rgba(162, 135, 135, 0.9)",
                                "z-index": 10,
                                "padding": 0,
                                "margin": 0
                            }
                        }
                    }
                }

            },

            "": {
                "redirect": "/home"
            },
            "/": {
                "redirect": "/home"
            },
            "/home": {
                "type": "virtual-list",
                "hasparam": false,
                "template": "list",
                "virtualrepeat": true,
                "api": {
                    "hasdata": true,
                    "multiple": true,
                    //put in {language where you want your language slug}
                    "url": "http://local.rienkcmswp.com/{language}/wp-json/wp/v2/posts",
                    "type": "Get",
                    "datakeys":[],
                    //keys are the api return keys
                    //so if the api returns title as {title: {rendered: 'your title'}} insert "titlekey": ["title", "rendered"]
                    "idkey": ['id'],
                    "idkey": ['id'],
                    "contentkey": ["content", "rendered"],
                    "titlekey": ["title", "rendered"],
                    "excerptkey": ["excerpt", "rendered"],
                    "authorkey": ["author"],
                    "categorieskey": ['categories'],
                    "slugkey": [ 'slug']
             
                },
                "defaultdata": {
                    "pagetitle": "",
                    "content": []
                },
                "elements": [{
                    "type": "header",
                    "value": {
                        "content":"homeheader",
                        "show": true,
                        "isfixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "100px",
                                    "background-color": "rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 90,
                                    "height": "90px",
                                    "background-color":"rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 80,
                                    "height": "80px",
                                    "background-color":"rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "70px",
                                    "background-color": "rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 60,
                                    "height": "60px",
                                    "background-color": "rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "breadcrumbs",
                    "value": {
                        "show": true,
                        "isfixed": true,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "pagetitle",
                    "value": {
                        "show": true,
                        "isfixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "70px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0

                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 60,
                                    "height": "60px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "contentrow",
                    "value": {
                        "show": true,
                        "isFixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 200,
                                    "height": "200px"
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 160,
                                    "height": "160px"
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 140,
                                    "height": "140px"
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "120px"
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "100px"
                                }
                            }
                        }
                    }
                }, {
                    "type": "footer",
                    "value": {
                        "show": true,
                        "isfixed": true,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 40,
                                    "height": "40px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 45,
                                    "height": "45px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 60,
                                    "height": "60px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "70px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }]
            },
            "/detail": {
                "type": "lazy-render",
                "hasparam": false,
                "template": "detail",
                "virtualrepeat": true,
                "api": {
                    "hasdata": true,
                    "multiple": false,
                    "url": "http://local.rienkcmswp.com/{language}/wp-json/wp/v2/posts/{slug}",
                    "type": "Get",
                    "datakeys":[],
                    //keys are the api return keys
                    "idkey": ['id'],
                    "contentkey": ["content", "rendered"],
                    "titlekey": ["title", "rendered"],
                    "excerptkey": ["excerpt", "rendered"],
                    "authorkey": ["author"],
                    "categorieskey": ['categories'],
                    "slugkey": ['slug']
                },
                "defaultdata": {
                    "pagetitle": "",
                    "content": []
                },
                "elements": [{
                    "type": "header",
                    "value": 'default',/*{
                        "show": true,
                        "isfixed": true,
                        "content":"<h1>RienkCMS Header</h1>",
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color":"rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "70px",
                                    "background-color":"rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "100px",
                                    "background-color": "rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "120px",
                                    "background-color": "rgba(193, 176, 176, 0.95)",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }*/
                }, {
                    "type": "breadcrumbs",
                    "value": 'default'/*{
                        "show": true,
                        "isfixed": true,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 20,
                                    "height": "20px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 20,
                                    "height": "20px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 20,
                                    "height": "20px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 20,
                                    "height": "20px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 20,
                                    "height": "20px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }*/
                }, {
                    "type": "pagetitle",
                    "value": {
                        "show": true,
                        "isfixed": true,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0

                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 60,
                                    "height": "60px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "70px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 80,
                                    "height": "80px",
                                    "background-color": "rgba(150, 133, 133, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "contentblock",
                    "value": {
                        "show": true,
                        "isFixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            }
                        }
                    }
                }, {
                    "type": "footer",
                    "value": {
                        "show": true,
                        "isfixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 40,
                                    "height": "40px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 45,
                                    "height": "45px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 60,
                                    "height": "60px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "70px",
                                    "background-color": "rgba(162, 135, 135, 0.9)",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }]
            }
        }
    }
};


var configdummyapi = {
    "app": {
    	languages: [

        {val: 'EN', text: 'english'}, 

        {val:'NL', text: 'Dutch'}

        ],
        "title": "rienkCMS",
        "breadcrumbbase": "rienkCMS",
        "apitype": "dummyapi",
        "breakpoints": {
            "base": {
                "minWidth": 0
            },
            "sm": {
                "minWidth": 568
            },
            "md": {
                "minWidth": 768
            },
            "lg": {
                "minWidth": 1024
            },
            "xl": {
                "minWidth": 1280
            }
        },
        "contentwidth": {
            "base": {
                "width": null
            },
            "sm": {
                "width": null
            },
            "md": {
                "width": null
            },
            "lg": {
                "width": 768
            },
            "xl": {
                "width": 1024
            }
        },
        "pages": {
            "": {
                "redirect": "/home"
            },
            "/": {
                "redirect": "/home"
            },
            "/home": {
                "type": "virtual-list",
                "hasparam": false,
                "template": "list",
                "virtualrepeat": true,
                "api": {
                    "hasdata": true,
                    "url": "http://local.rienkcms.com/api/data",
                    "type": "Get",
                    "datakeys": ['data'],
                },
                "defaultdata": {
                    "pagetitle": "",
                    "content": []
                },
                "elements": [{
                    "type": "header",
                    "value": {
                        "content":"<h1>RienkCMS Header</h1>",
                        "show": true,
                        "isfixed": true,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "breadcrumbs",
                    "value": {
                        "show": true,
                        "isfixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "pagetitle",
                    "value": {
                        "show": true,
                        "isfixed": true,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "contentrow",
                    "value": {
                        "show": true,
                        "isFixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px"
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px"
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px"
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px"
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px"
                                }
                            }
                        }
                    }
                }, {
                    "type": "footer",
                    "value": {
                        "show": true,
                        "isfixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 9999,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }]
            },
            "/detail": {
                "type": "lazy-render",
                "hasparam": false,
                "template": "detail",
                "virtualrepeat": true,
                "api": {
                    "hasdata": true,
                    "url": "http://local.rienkcms.com/api/sdata",
                    "type": "Get",
                    "datakeys": ['data']
                },
                "defaultdata": {
                    "pagetitle": "",
                    "content": []
                },
                "elements": [{
                    "type": "header",
                    "value": {
                        "show": true,
                        "isfixed": true,
                        "content":"<h4>RienkCMS Header</h4>",
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "blue",
                                    "z-index": 30,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "breadcrumbs",
                    "value": {
                        "show": true,
                        "isfixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "grey",
                                    "z-index": 20,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "pagetitle",
                    "value": {
                        "show": true,
                        "isfixed": true,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }, {
                    "type": "contentblock",
                    "value": {
                        "show": true,
                        "isFixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": null,
                                    "height": "auto"
                                }
                            }
                        }
                    }
                }, {
                    "type": "footer",
                    "value": {
                        "show": true,
                        "isfixed": false,
                        "css": {
                            "base": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "sm": {
                                "rules": {
                                    "intheight": 50,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "md": {
                                "rules": {
                                    "intheight": 70,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "lg": {
                                "rules": {
                                    "intheight": 100,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            },
                            "xl": {
                                "rules": {
                                    "intheight": 120,
                                    "height": "50px",
                                    "background-color": "green",
                                    "z-index": 10,
                                    "padding": 0,
                                    "margin": 0
                                }
                            }
                        }
                    }
                }]
            }
        }
    }
};
/*
var config = {
	
	apitype: 'dummyapi',

	datakey: 'data',
	
	languages: [

	{val: 'EN', text: 'english'}, 

	{val:'NL', text: 'Dutch'}],

	getconfig: 'getconfig';

	//{language} is the val provided above necessary for language api calls
	getallposts: 'data',

	getpost: 'sdata/{id}'
	

}


*/

var translations = {

    EN: {

        'english' : 'English',
        'dutch' : 'Dutch',
        'homeheader' : '<h1>RienkCMS Header EN</h1>',
        'home' : 'Home'



    },

    NL: {

        'english' : 'Engels',
        'dutch': 'Nederlands',
        'homeheader' : '<h1>RienkCMS Header NL</h1>',
        'home' : 'Thuis'
    }


}


window.config = configwordpress;

window.translations = translations;
//creates a dummy api no backend needed
window.config.dummmy = true;