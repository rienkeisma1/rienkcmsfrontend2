import { Rienkcmsfrontend2Page } from './app.po';

describe('rienkcmsfrontend2 App', function() {
  let page: Rienkcmsfrontend2Page;

  beforeEach(() => {
    page = new Rienkcmsfrontend2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
